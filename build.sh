#!/bin/sh

# If you're not using the GitLab CI build, this script will produce similar output...

make clean
make html
make text
