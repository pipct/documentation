# Network services provided by FSMG

## rsync

Plaintext anonymous rsync is available from [rsync://mirror.fsmg.org.nz](rsync://mirror.fsmg.org.nz). The rsync paths are configured by Ansible and should match what is available via the web.

## HTTP and HTTPS

Both HTTP and HTTPS access is available via [https://mirror.fsmg.org.nz/](https://mirror.fsmg.org.nz/). While we would encourage as many people as possible to use HTTPS we also fully support HTTP for those systems where HTTPS mirroring is not yet considered necessary.

To allow for identical configuration between HTTP and HTTPs the configuration for the sites themselves is stored in `/etc/apache2/includes/mirror.conf` and `/etc/apache2/includes/website.conf`. These files are managed by Ansible.

If you visit one of the direct node hostnames it will still work (to allow for monitoring by our upstreams) but you will see a banner asking you to use the anycast hostname.

Both nodes are also configured to answer for `ftp.nz.debian.org` as we are candidates for this service, but obviously HTTPS will not work there (and that is fine)

### Apache configuration

Because most HTTPS connections to FSMG are relatively long lived we need to increase the maximum number of simultaneous connections to deal with peak load.

Currently we configure `mpm_event` with:

````
<IfModule mpm_event_module>
	StartServers            3
	MinSpareThreads         25
	MaxSpareThreads         75
	ThreadLimit             64
	ThreadsPerChild         25
	MaxRequestWorkers       200
	MaxConnectionsPerChild  0
</IfModule>
````

### HTTPS certificates

Certbot is used to issue Let's Encrypt certificates. It's configured to use a DNS API hook with Catalyst to perform `dns-01` challenges.

In `/etc/letsencrypt/cli.ini` this is set up as:

```
rsa-key-size = 4096
preferred-challenges = dns
manual-auth-hook = /opt/fsmg/bin/catalyst-certbot-hook auth
manual-cleanup-hook = /opt/fsmg/bin/catalyst-certbot-hook cleanup
manual-public-ip-logging-ok = True
```

The Catalyst certbot hook is kept at [https://gitlab.com/fsmg/private/blob/master/catalyst-certbot-hook](https://gitlab.com/fsmg/private/blob/master/catalyst-certbot-hook).

## FTP

We will not be providing FTP service.

## SSH for push mirroring

We accept SSH to our anycast address to allow for "push mirroring" for projects that support it, and to our individual node addresses for those projects who preferred that.
