# Hosted equipment

FSMG has two Sun Microsystems SunFire X2270 servers. These are 1U machines with six Intel(R) Xeon(R) CPU           X5660 CPU cores and 32GiB of RAM each.

* The node called "wlglam" has the serial number 0328MSL-0921660383.
* The node called "hlzmel" has the serial number 0328MSL-09236603BU.

These machines were donated by [Catalyst IT](https://catalyst.net.nz/).

The drives for these machines were donated by [Insomnia Security](https://www.insomniasec.com/) and additional hardware was donated by [ZX Security](https://zxsecurity.co.nz/), [Advantage](https://advantage.nz/) and Haydn Greatnews.

## Disks

Each machine has four WDC WD60EFRX-68L0BN1 6TB disks.

In "wlglam":

````
/dev/sda: WD-WX11D278Z5S5
/dev/sdb: WD-WX11D278Z1DZ
/dev/sdc: WD-WX11D278ZU9J
/dev/sdd: WD-WX11D278Z57Z
````

In "hlzmel":

````
/dev/sda: WD-WX11D278Z8N8
/dev/sdb: WD-WX11D278ZT2L
/dev/sdc: WD-WX11D278ZR84
/dev/sdd: WD-WX11D278Z83J
````

Spare disks [are held in Wellington](spares.md).

More information about the disk configuration is available on the [filesystems](filesystem.md) page.

## RAM

In "hlzmel" we have four Micron 36KSF1G72PZ-1G4M1 8GiB DDR3-1333 ECC Registered DIMMs. In "wlglam" we have one further Micron DIMM and three equivalent Kingston DIMMs.

## Labelling

The machines are very clearly labeled on the front, back and top with FSMG contact details.

## Locations

The "wlglam" node is hosted in the CityLink facility on Lambton Quay in space leased by [REANNZ](service_providers.md).

The "hlzmel" node is hosted in the Vocus HNINNO facility on Melody Lane in space leased by REANNZ. Our contact there is Brad, and the REANNZ contacts.

## Hardware quirks

The "wlglam" node had a failed power LED, so this LED has been replaced with an extremely bright blue LED. You'll know it when you see it.

