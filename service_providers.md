# Third party service providers

## GitLab

[https://gitlab.com/fsmg](https://gitlab.com/fsmg) is a free account run by Brad, James and Michael. All members of the account must have 2FA enabled in GitLab.

GitLab is where we collaborate on documentation, tools, scripts and ansible playbooks.

## E-mail hosting

E-mail hosting is currently provided by Michael on a best effort basis. There is a [mirror-operators@fsmg.org.nz](mailto:mirror-operators@fsmg.org.nz) mailing list hosted on the [Hotplate Labs mailman server](https://lists.hotplate.co.nz/listinfo/mirror-operators).

These aliases exist:

```
brad.cowie@fsmg.org.nz brad@c[xxxx].nz
michael.fincham@fsmg.org.nz michael@h[xxxxxxx].co.nz
contact@fsmg.org.nz mirror-operators@fsmg.org.nz
sponsorship@fsmg.org.nz mirror-operators@fsmg.org.nz
mirror-operators-admin@fsmg.org.nz mirror-operators-admin@lists.hotplate.co.nz
mirror-operators-bounces@fsmg.org.nz mirror-operators-bounces@lists.hotplate.co.nz
mirror-operators-confirm@fsmg.org.nz mirror-operators-confirm@lists.hotplate.co.nz
mirror-operators-join@fsmg.org.nz mirror-operators-join@lists.hotplate.co.nz
mirror-operators-leave@fsmg.org.nz mirror-operators-leave@lists.hotplate.co.nz
mirror-operators-owner@fsmg.org.nz mirror-operators-owner@lists.hotplate.co.nz
mirror-operators-request@fsmg.org.nz mirror-operators-request@lists.hotplate.co.nz
mirror-operators-subscribe@fsmg.org.nz mirror-operators-subscribe@lists.hotplate.co.nz
mirror-operators-unsubscribe@fsmg.org.nz mirror-operators-unsubscribe@lists.hotplate.co.nz
mirror-operators@fsmg.org.nz mirror-operators@lists.hotplate.co.nz
security@fsmg.org.nz mirror-operators@fsmg.org.nz
```

These mailboxes exist:

```
james.forman@fsmg.org.nz
```

## Domain registration

This is provided as part of a sponsorship deal with [Metaname]](https://metaname.net). Our Metaname username is "4mkm", and currently Michael has the credentials to log in to this, and the 2FA token.

## DNS

Provided by Catalyst. The API key is stored in `/opt/fsmg/bin/pdns-cli/conf.toml` on both servers.

## Rackspace and connectivity

REANNZ kindly provides rackspace and connectivity.

Contact information for REANNZ is available at [https://gitlab.com/fsmg/private/blob/master/reannz](https://gitlab.com/fsmg/private/blob/master/reannz).

## Monitoring and alerting

Currently monitoring is through James's Icinga2 installation at [https://monitoring.jfnet.nz/](https://monitoring.jfnet.nz/). James sponsors the alerting for FSMG.

## TLS

Let's Encrypt is used to issue https certificates.
