# This documentation

The documentation for FSMG is built using Sphinx with markdown.

The canonical source of the documentation is [https://gitlab.com/fsmg/documentation](https://gitlab.com/fsmg/documentation). When this repo is updated a GitLab CI hook recompiles the documentation and publishes it at [https://fsmg.gitlab.io/documentation/](https://fsmg.gitlab.io/documentation/).

## Updating the documentation

Please be cautious with the documentation as it will be made public. There is a `private` repo in GitLab for anything which needs to not be published.